﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data as List<T>;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public async Task<T> AddAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(AddAsync)} entity must not be null");
            }

            try
            {
                Data.Add(entity);
                return await Task.FromResult(entity);
            }
            catch (Exception)
            {
                throw new Exception($"{nameof(entity)} could not be saved");
            }
        }

        public async Task<T> UpdateAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(UpdateAsync)} entity must not be null");
            }

            try
            {
                var index = Data.IndexOf(Data.FirstOrDefault(x => x.Id == entity.Id));

                if (index != -1)
                {
                    Data[index] = entity;
                }
                else
                {
                    throw new ArgumentNullException($"{nameof(UpdateAsync)} entity not exists in memory");
                }

                return await Task.FromResult(entity); ;
            }
            catch (Exception)
            {
                throw new Exception($"{nameof(entity)} could not be updated");
            }
        }

        public void Remove(Guid id)
        {
            var entity = Data.FirstOrDefault(x => x.Id == id);
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(Remove)} entity is not exists");
            }

            try
            {
                Data.Remove(entity);
            }
            catch (Exception)
            {
                throw new Exception($"{nameof(entity)} could not be removed");
            }
        }
    }
}